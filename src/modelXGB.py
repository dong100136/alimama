import pandas as pd
import numpy as np
from pandas import Series, DataFrame
from datetime import datetime
from xgboost.sklearn import XGBClassifier
from sklearn.metrics import log_loss
from sklearn import preprocessing
from combineData import get_final_data
import utils


class ModelXGB(object):
    def __init__(self):
        self.model = xgb = XGBClassifier(
            silent=1,
            learning_rate=0.01,
            n_estimators=1282,
            max_depth=5,
            min_child_weight=7,
            gamma=0,
            subsample=0.7,
            colsample_bytree=0.7,
            objective='binary:logistic',
            scale_pos_weight=1,
            reg_alpha=0.001,
            seed=27)

    def fit(self, X, y):
        self.model.fit(X, y, eval_metric='logloss')

    def predict(self, data):
        y_predict = self.model.predict_proba(data)[:, 1]
        return y_predict


if __name__ == "__main__":

    # 列出训练特征
    features = []
    # 广告相关的特征
    features.extend(['item_id', 'item_brand_id', 'item_city_id', 'item_price_level', 'item_sales_level',
                     'item_collected_level', 'item_pv_level', 'item_category_0', 'item_category_1', 'item_category_2', ])
    # 用户相关的特征
    features.extend(['user_gender_id', 'user_occupation_id',
                     'user_age_level', 'user_star_level', ])
    # 上下文相关的特征
    features.extend(
        ['context_page_id', 'context_timestamp_Hour', 'context_timestamp_weekday'])
    # 店铺相关的特征
    features.extend(['shop_id', 'shop_review_num_level', 'shop_star_level',
                     'shop_review_positive_rate', 'shop_score_service', 'shop_score_delivery', 'shop_score_description', ])
    # pcvr特征
    features.extend([
        'item_category_1_pcvr', 'item_category_1_C', 'item_category_1_I',
        'item_category_2_pcvr', 'item_category_2_C', 'item_category_2_I',
        'item_brand_id_pcvr', 'item_brand_id_C', 'item_brand_id_I',
        'item_city_id_pcvr', 'item_city_id_C', 'item_city_id_I',
        'item_price_level_pcvr', 'item_price_level_C', 'item_price_level_I',
        'item_sales_level_pcvr', 'item_sales_level_C', 'item_sales_level_I',
        'item_collected_level_pcvr', 'item_collected_level_C', 'item_collected_level_I',
        'item_pv_level_pcvr', 'item_pv_level_C', 'item_pv_level_I',
        'user_gender_id_pcvr', 'user_gender_id_C', 'user_gender_id_I',
        'user_age_level_pcvr', 'user_age_level_C', 'user_age_level_I',
        'user_occupation_id_pcvr', 'user_occupation_id_C', 'user_occupation_id_I',
        'user_star_level_pcvr', 'user_star_level_C', 'user_star_level_I',
        'context_timestamp_Hour_pcvr', 'context_timestamp_Hour_C', 'context_timestamp_Hour_I',
        'context_page_id_pcvr', 'context_page_id_C', 'context_page_id_I',
        'shop_review_num_level_pcvr', 'shop_review_num_level_C', 'shop_review_num_level_I',
        'shop_star_level_pcvr', 'shop_star_level_C', 'shop_star_level_I'])

    data = get_final_data()
    utils.local_test(data, features, ModelXGB)

    # train,test = utils.get_online_train_test(data)
    # model2 = ModelLGB()
    # model2.fit(train[features],train['is_trade'])
    # test['predicted_score'] = model.predict(test[features])
    # test[['instance_id','predicted_score']].to_csv(utils.result_out_path,sep=" ",index=False)