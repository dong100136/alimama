import pandas as pd  
import numpy as np  
from scipy import sparse  
from sklearn.preprocessing import OneHotEncoder  
from sklearn.linear_model import LogisticRegression  
from sklearn.preprocessing import StandardScaler  
from sklearn.preprocessing import LabelEncoder 
from sklearn.metrics import log_loss 
import pickle


train_read = open('../data/valid_train_handled.pkl','rb')
train = pickle.load(train_read)
test_read = open('../data/valid_test_handled.pkl','rb')
test = pickle.load(test_read)


df_train = train 
df_test  = test
y_train = df_train['is_trade'].values  
y_test = df_test['is_trade'].values  

# 2. process data  
ss = StandardScaler()  
enc = OneHotEncoder()  
lbl = LabelEncoder()


## 类别特征
category_features = ['category_last','item_city_id','item_brand_id','shop_id','item_id',
         'context_timestamp_Hour','user_gender_id','user_occupation_id',
        ]


for i,feat in enumerate(category_features):
    temp = lbl.fit_transform((list(df_train[feat])+list(df_test[feat])))
    enc.fit(temp.reshape(-1,1))
    x_train = enc.transform(lbl.transform(df_train[feat]).reshape(-1,1))
    x_test = enc.transform(lbl.transform(df_test[feat]).reshape(-1,1))
    if i == 0:
        X_train ,X_test = x_train , x_test
    else:
        X_train ,X_test = sparse.hstack((X_train,x_train)),sparse.hstack((X_test,x_test))
        
## 连续特征
continue_features = [
                'category_pcvr',
                #'brand_pcvr','brand_C','brand_I',
                'city_pcvr',
                'price_pcvr',
                'sales_pcvr',
                'collected_pcvr',
                'pv_pcvr',
                'gender_pcvr',
                'age_pcvr',
                'occupation_pcvr',
                'userstar_pcvr',
                'hour_pcvr',
                'page_pcvr',
                'shop_review_pcvr',
                'shopstar_pcvr',]        

X_train ,X_test = sparse.hstack((X_train,df_train[continue_features])),sparse.hstack((X_test,df_test[continue_features]))

lr = LogisticRegression()  
lr.fit(X_train, y_train)  
proba_test = lr.predict_proba(X_test)[:, 1]  
print ("logloss Score (Test): %f" % log_loss(df_test['is_trade'], proba_test))

##########################################


## 需要标准化
standar_features = [
    'category_C','category_I',
    #'city_C','city_I',
    #'price_C','price_I',
    'sales_C','sales_I',
    'collected_C','collected_I',
    'pv_C','pv_I',
    'gender_C','gender_I',
    'age_C','age_I',
    'occupation_C','occupation_I',
    'userstar_C','userstar_I',
    'hour_C','hour_I',
    'page_C','page_I',
    'shop_review_C','shop_review_I',
    'shopstar_C','shopstar_I', 
]
scaler = ss.fit(df_train[standar_features].values)
x_train = scaler.transform(df_train[standar_features].values)
x_test = scaler.transform(df_test[standar_features].values)
X_train, X_test = sparse.hstack((X_train, x_train)), sparse.hstack((X_test, x_test))
