
# coding: utf-8

# In[31]:


import pandas as pd
import numpy as np
from ffm import FFM, FFMFormatPandas, FFMData, save_data, load_data


# In[32]:


train = pd.read_table("../data/round1_ijcai_18_train_20180301.txt",delim_whitespace=True)
test = pd.read_table("../data/round1_ijcai_18_test_a_20180301.txt",delim_whitespace = True)


# In[33]:


data = pd.concat([train,test])


# In[34]:


## 类别特征

cate_features = ['context_page_id','item_brand_id','item_city_id','item_collected_level','item_id','item_price_level',
                'item_pv_level','item_sales_level','shop_id','shop_review_num_level','shop_star_level','user_age_level',
                'user_gender_id','user_occupation_id','user_star_level','item_property_list','item_category_list',
                'context_hour']

## 连续特征
continue_features = ['shop_review_positive_rate','shop_score_delivery','shop_score_description','shop_score_service',
                    'same_property_prob','same_category_prob']


## 无需处理的无用特征
drop_features = ['context_id','instance_id','user_id',]


# In[35]:


from sklearn.preprocessing import OneHotEncoder  
from sklearn.preprocessing import StandardScaler  
from sklearn.preprocessing import LabelEncoder 
ss = StandardScaler()  
enc = OneHotEncoder()  
lbl = LabelEncoder()


# In[37]:


def split_category(x,index):
    tmp  = []
    for a in x:
        tmp.append(a.split(':',1)[index])
    return tmp  

def split_property(x):
    tmp  = []
    for a in x:
        tmp.extend(a.split(',',a.count(",")))
    return tmp 

def check_same(a,b):
    set_same = a & b
    return list(set_same)
## 逐步切分预测类别特征
data['predict_category_list'] = data['predict_category_property'].apply(lambda x:x.split(';',x.count(';')))
data['predict_category'] = data['predict_category_list'].apply(lambda x:split_category(x,0))
data['predict_category'] = data['predict_category'].apply(lambda x:set(x))
## 切分广告商品类别特征
data['item_category'] = data['item_category_list'].apply(lambda x:x.split(';',x.count(';')))
data['item_category'] = data['item_category'].apply(lambda x:set(x))
## 筛出商品类别和预测类别的交集
data['same_category_list'] = list(map(check_same,data['predict_category'], data['item_category']))
## 计算预测类别和广告商品类别的相似程度
data['same_category_num'] = data['same_category_list'].apply(lambda x:len(x))
data['predict_category_num'] = data['predict_category'].apply(lambda x:len(x))
data['same_category_prob'] = data['same_category_num']/data['predict_category_num']

## 逐步切分预测属性特征
data['predict_property'] = data['predict_category_list'].apply(lambda x:split_category(x,-1))
data['predict_property'] = data['predict_property'].apply(lambda x:split_property(x))
data['predict_property']= data['predict_property'].apply(lambda x:set(x))
## 切分广告商品属性特征
data['item_property'] = data['item_property_list'].apply(lambda x:x.split(';',x.count(';')))
data['item_property'] = data['item_property'].apply(lambda x:set(x))
## 筛出商品属类别和预测类别的交集
data['same_property_list'] = list(map(check_same,data['predict_property'], data['item_property']))
## 计算预测属性和广告商品属性的相似程度
data['same_property_num'] = data['same_property_list'].apply(lambda x:len(x))
data['predict_property_num'] = data['predict_property'].apply(lambda x:len(x))
data['same_property_prob'] = data['same_property_num']/data['predict_property_num']


# In[38]:


from datetime import datetime
def create_time_feature(df):
    """
    构建时间特征 月 日 时 
    """
    df['context_timestamp'] = df.apply(lambda x : x.context_timestamp+28800,axis=1)
    df['context_timestamp_datetype'] = pd.to_datetime(df['context_timestamp'],unit='s')
    df['context_day'] = [datetime.strftime(x,'%d') for x in df['context_timestamp_datetype']]
    df['context_hour'] = [datetime.strftime(x,'%H') for x in df['context_timestamp_datetype']]
    df['context_day'] = df['context_day'].astype('int')
    df['context_hour'] = df['context_hour'].astype('int')
    return df


# In[39]:


## 构建时间相关特征
data = create_time_feature(data)


# In[40]:


## 将类别属性编码
for i,feat in enumerate(cate_features):
    data[feat] = lbl.fit_transform(data[feat])


# In[41]:


ffm_train = FFMFormatPandas()
ffm_train.fit(data, target='is_trade', categorical=cate_features, numerical=continue_features)


# In[46]:


train = data[:478137]
test = data[478138:]


# In[52]:


## 划分测试集
valid = train.loc[(train.context_day == 23)]
train = train.loc[(train.context_day <= 22)&(train.context_day >= 18)]  


# In[53]:


## 训练集转换为ffm可以接收的数据格式
train_data = ffm_train.transform_convert(train)


# In[67]:


## 测试集转换为ffm可以接收的数据格式
valid_data = ffm_train.transform_convert(valid)


# In[68]:


model = FFM(eta=0.3, lam=0.0001, k=4)
model.fit(train_data, num_iter=32, val_data=valid_data, metric='logloss', early_stopping=20, maximum=True) 


# In[69]:


val_proba = model.predict_proba(valid_data)


# In[71]:


from sklearn.metrics import log_loss
print ("logloss Score (FFM): %f" % log_loss(valid['is_trade'], val_proba))

