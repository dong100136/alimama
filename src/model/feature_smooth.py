import pandas as pd
import numpy as np 
from pandas import Series,DataFrame
from datetime import datetime
from sklearn.ensemble import RandomForestClassifier
import lightgbm as lgb
from sklearn.metrics import log_loss
import pickle
from sklearn import preprocessing
import numpy
import random
import scipy.special as special

numpy.random.seed(0)
lbl = preprocessing.LabelEncoder()

'''''
一. 加入历史pcvr后logloss提升原因分析
  1. 查看一些训练集数据和测试集数据有null值
  2. 数据不够平滑，有基数小的分类会导致历史pcvr异常大    ---  TODO:贝叶斯平滑处理

  去除历史pcvr和用户点击特征后logloss  0.083628674831
  去除历史pcvr后logloss              0.0831499240088
                                   0.0832428037895


   添加广告符合程度特征               0.0831210360444

   num_leaves=31, n_estimators=100, n_jobs=20  +shopstar 0.0830612449626


二、需要确定lgm模型输入特征需要怎么样的预处理，不能一味添加特征

三、要把属性特征转化成 category feature  
   转化后作为类别特征，但是无效果

四、brand这个特征加入后影响很大

'''''




class BayesianSmoothing(object):
    def __init__(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta

    def sample(self, alpha, beta, num, imp_upperbound):
        sample = numpy.random.beta(alpha, beta, num)
        I = []
        C = []
        for clk_rt in sample:
            imp = random.random() * imp_upperbound
            imp = imp_upperbound
            clk = imp * clk_rt
            I.append(imp)
            C.append(clk)
        return I, C

    def update(self, imps, clks, iter_num, epsilon):
        for i in range(iter_num):
            new_alpha, new_beta = self.__fixed_point_iteration(imps, clks, self.alpha, self.beta)
            if abs(new_alpha-self.alpha)<epsilon and abs(new_beta-self.beta)<epsilon:
                break
            #print (new_alpha,new_beta,i)
            self.alpha = new_alpha
            self.beta = new_beta

    def __fixed_point_iteration(self, imps, clks, alpha, beta):
        numerator_alpha = 0.0
        numerator_beta = 0.0
        denominator = 0.0

        for i in range(len(imps)):
            numerator_alpha += (special.digamma(clks[i]+alpha) - special.digamma(alpha))
            numerator_beta += (special.digamma(imps[i]-clks[i]+beta) - special.digamma(beta))
            denominator += (special.digamma(imps[i]+alpha+beta) - special.digamma(alpha+beta))

        return alpha*(numerator_alpha/denominator), beta*(numerator_beta/denominator)

def split_category(x,index):
    tmp  = []
    for a in x:
        tmp.append(a.split(':',1)[index])
    return tmp  

def split_property(x):
    tmp  = []
    for a in x:
        tmp.extend(a.split(',',a.count(",")))
    return tmp 

def check_same(a,b):
    set_same = a & b
    return list(set_same)

def create_time_feature(df):
    """
    构建时间特征 月 日 时 
    """
    df['context_timestamp'] = df.apply(lambda x : x.context_timestamp+28800,axis=1)
    df['context_timestamp_datetype'] = pd.to_datetime(df['context_timestamp'],unit='s')
    df['context_timestamp_day'] = [datetime.strftime(x,'%d') for x in df['context_timestamp_datetype']]
    df['context_timestamp_Hour'] = [datetime.strftime(x,'%H') for x in df['context_timestamp_datetype']]
    df['context_timestamp_day'] = df['context_timestamp_day'].astype('int')
    df['context_timestamp_Hour'] = df['context_timestamp_Hour'].astype('int')
    return df

def create_user_query_data(data):
    user_query_day = data.groupby(['user_id', 'context_timestamp_day']).size(
    ).reset_index().rename(columns={0: 'user_query_day'})
    data = pd.merge(data, user_query_day, 'left', on=['user_id', 'context_timestamp_day'])
    user_query_day_hour = data.groupby(['user_id', 'context_timestamp_day', 'context_timestamp_Hour']).size().reset_index().rename(
        columns={0: 'user_query_day_hour'})
    data = pd.merge(data, user_query_day_hour, 'left',
                    on=['user_id', 'context_timestamp_day', 'context_timestamp_Hour'])

    shop_query_day=data.groupby(['context_timestamp_day','shop_id']).size().reset_index().rename(columns={0: 'shop_query_day'})
    data = pd.merge(data, shop_query_day, 'left', on=['shop_id', 'context_timestamp_day'])
    shop_query_day_hour=data.groupby(['context_timestamp_day','context_timestamp_Hour','shop_id']).size().reset_index().rename(columns={0: 'shop_query_day_hour'})
    data = pd.merge(data, shop_query_day_hour, 'left', on=['shop_id', 'context_timestamp_day','context_timestamp_Hour'])
    
    item_query_day=data.groupby(['context_timestamp_day','item_id']).size().reset_index().rename(columns={0: 'item_query_day'})
    data = pd.merge(data, item_query_day, 'left', on=['item_id', 'context_timestamp_day'])
    item_query_day_hour=data.groupby(['context_timestamp_day','context_timestamp_Hour','item_id']).size().reset_index().rename(columns={0: 'item_query_day_hour'})
    data = pd.merge(data, item_query_day_hour, 'left', on=['item_id', 'context_timestamp_day','context_timestamp_Hour'])
    return data  

def prepare_data(data):
    ## 商品特征:category_last
    lbl = preprocessing.LabelEncoder()
    data['category_last'] = data['item_category_list'].apply(lambda x:x.split(';',x.count(';'))[-1])
    data['category_last'] = data['category_last'].astype('int')
    ## 类别编码
    data['item_brand_id_enc'] = lbl.fit_transform(data['item_brand_id'])
    data['item_city_id_enc'] = lbl.fit_transform(data['item_city_id'])
    data['shop_id_enc'] = lbl.fit_transform(data['shop_id'])
    data['category_last_enc'] = lbl.fit_transform(data['category_last'])
    ## 是否需要编码？？ 会默认把缺失值当作一类分类处理
    data['shop_star_level_enc'] = lbl.fit_transform(data['shop_id'])
    data['context_page_enc'] = lbl.fit_transform(data['context_page_id'])
    data['user_occupation_id_enc'] = lbl.fit_transform(data['user_occupation_id'])
    data['user_star_level_enc'] = lbl.fit_transform(data['user_star_level'])
    data['user_age_level_enc'] = lbl.fit_transform(data['user_age_level'])
    
    ## 上下文特征: context_timestamp_day 
    ##           context_timestamp_Hour
    create_time_feature(data)

    ## 商品特征:same_property_num
    ##         same_property_list
    ##         same_category_num
    ##         same_category_list
    ## 逐步切分预测类别特征
    ## 逐步切分预测类别特征
    data['predict_category_list'] = data['predict_category_property'].apply(lambda x:x.split(';',x.count(';')))
    data['predict_category'] = data['predict_category_list'].apply(lambda x:split_category(x,0))
    data['predict_category'] = data['predict_category'].apply(lambda x:set(x))
    ## 切分广告商品类别特征
    data['item_category'] = data['item_category_list'].apply(lambda x:x.split(';',x.count(';')))
    data['item_category'] = data['item_category'].apply(lambda x:set(x))
    ## 筛出商品类别和预测类别的交集
    data['same_category_list'] = list(map(check_same,data['predict_category'], data['item_category']))
    ## 计算预测类别和广告商品类别的相似程度
    data['same_category_num'] = data['same_category_list'].apply(lambda x:len(x))
    data['predict_category_num'] = data['predict_category'].apply(lambda x:len(x))
    data['same_category_prob'] = data['same_category_num']/data['predict_category_num']

    ## 逐步切分预测属性特征
    data['predict_property'] = data['predict_category_list'].apply(lambda x:split_category(x,-1))
    data['predict_property'] = data['predict_property'].apply(lambda x:split_property(x))
    data['predict_property']= data['predict_property'].apply(lambda x:set(x))
    ## 切分广告商品属性特征
    data['item_property'] = data['item_property_list'].apply(lambda x:x.split(';',x.count(';')))
    data['item_property'] = data['item_property'].apply(lambda x:set(x))
    ## 筛出商品属类别和预测类别的交集
    data['same_property_list'] = list(map(check_same,data['predict_property'], data['item_property']))
    ## 计算预测属性和广告商品属性的相似程度
    data['same_property_num'] = data['same_property_list'].apply(lambda x:len(x))
    data['predict_property_num'] = data['predict_property'].apply(lambda x:len(x))
    data['same_property_prob'] = data['same_property_num']/data['predict_property_num']
    
    ## trick 特征 用户点击特征
    data = create_user_query_data(data)


    return data

 


def create_trade_precent(df,item_label,create_label):
    """
    构造商品历史成交转换率特征
    """
    trade_precent = pd.DataFrame(df.groupby([item_label,'is_trade']).size().reset_index().rename(columns={0:'item_trade_num'}))
    trade_precent = trade_precent[trade_precent['is_trade'] == 1]
    trade_num = pd.DataFrame(df.groupby([item_label]).size().reset_index().rename(columns={0:'item_num'}))
    trade_precent = pd.merge(trade_precent,trade_num,'left',[item_label])
    trade_precent = trade_precent.drop(['is_trade'],axis=1)
    trade_precent[create_label] = trade_precent['item_trade_num']/trade_precent['item_num']
    trade_precent = trade_precent.drop(['item_trade_num','item_num'],axis=1)
    df = pd.merge(df,trade_precent,'left',[item_label])
    return df,trade_precent


def create_trade_precent_smooth(df,item_label,create_label):
    """
    构造平滑商品历史成交转换率特征
    """
    I=df.groupby(item_label)['is_trade'].size().reset_index()
    I.columns = [item_label, create_label+'_I']
    C=df.groupby(item_label)['is_trade'].sum().reset_index()
    C.columns = [item_label, create_label+'_C']

    trade_precent = pd.concat([I, C[create_label+'_C']], axis=1)
    hyper = BayesianSmoothing(1, 1)
    hyper.update(trade_precent[create_label+'_I'].values, trade_precent[create_label+'_C'].values, 10000, 0.00000001)
    alpha = hyper.alpha
    beta = hyper.beta
    trade_precent[create_label+'_pcvr'] = (trade_precent[create_label+'_C'] + alpha) / (trade_precent[create_label+'_I'] + alpha + beta)
    df = pd.merge(df,trade_precent,'left',[item_label])
    return df,trade_precent


def prepare_train_test(train,test):
    """
    构造训练集统计特征
    """

    ## 商品类别-历史成交率
    train,category_pcvr = create_trade_precent_smooth(train,'category_last','category')
    ## 商品品牌-历史成交率
    train,brand_pcvr = create_trade_precent_smooth(train,'item_brand_id','brand')
    ## 商品城市-历史成交率
    train,city_pcvr = create_trade_precent_smooth(train,'item_city_id','city')
    ## 商品价格-历史成交率
    train,price_pcvr = create_trade_precent_smooth(train,'item_price_level','price')
    ## 商品销量-历史成交率
    train,sales_pcvr = create_trade_precent_smooth(train,'item_sales_level','sales')
    ## 商品收藏-历史成交率
    train,collected_pcvr = create_trade_precent_smooth(train,'item_collected_level','collected')
    ## 商品曝光率-历史成交率
    train,pv_pcvr = create_trade_precent_smooth(train,'item_pv_level','pv')
    ## 用户性别-历史成交率
    train,gender_pcvr = create_trade_precent_smooth(train,'user_gender_id','gender')
    ## 用户年龄-历史成交率
    train,age_pcvr = create_trade_precent_smooth(train,'user_age_level','age')
    ## 用户职业-历史成交率
    train,occupation_pcvr = create_trade_precent_smooth(train,'user_occupation_id','occupation')
    ## 用户等级-历史成交率
    train,userstar_pcvr = create_trade_precent_smooth(train,'user_star_level','userstar')
    ## 搜索时间-历史成交率
    train,hour_pcvr = create_trade_precent_smooth(train,'context_timestamp_Hour','hour')
    ## 搜索页面-历史成交率
    train,page_pcvr= create_trade_precent_smooth(train,'context_page_id','page')
    ## 店铺评价数量等级-历史成交率
    train,shop_review_pcvr = create_trade_precent_smooth(train,'shop_review_num_level','shop_review')
    ## 店铺等级-历史成交率
    train,shopstar_pcvr = create_trade_precent_smooth(train,'shop_star_level','shopstar')

    """
    构造测试集统计特征
    """
    ## 商品类别-历史成交率
    test = pd.merge(test,category_pcvr,'left',['category_last'])
    ## 商品品牌-历史成交率
    test = pd.merge(test,brand_pcvr,'left',['item_brand_id'])
    ## 商品城市-历史成交率
    test = pd.merge(test,city_pcvr,'left',['item_city_id'])
    ## 商品价格-历史成交率
    test = pd.merge(test,price_pcvr,'left',['item_price_level'])
    ## 商品销量-历史成交率
    test = pd.merge(test,sales_pcvr,'left',['item_sales_level'])
    ## 商品收藏-历史成交率
    test = pd.merge(test,collected_pcvr,'left',['item_collected_level'])
    ## 商品曝光率-历史成交率
    test = pd.merge(test,pv_pcvr,'left',['item_pv_level'])
    ## 用户性别-历史成交率
    test = pd.merge(test,gender_pcvr,'left',['user_gender_id'])
    ## 用户年龄-历史成交率
    test = pd.merge(test,age_pcvr,'left',['user_age_level'])
    ## 用户职业-历史成交率
    test = pd.merge(test,occupation_pcvr,'left',['user_occupation_id'])
    ## 用户等级-历史成交率
    test = pd.merge(test,userstar_pcvr,'left',['user_star_level'])
    ## 搜索时间-历史成交率
    test = pd.merge(test,hour_pcvr,'left',['context_timestamp_Hour'])
    ## 搜索页面-历史成交率
    test = pd.merge(test,page_pcvr,'left',['context_page_id'])
    ## 店铺评价数量等级-历史成交率
    test = pd.merge(test,shop_review_pcvr,'left',['shop_review_num_level'])
    ## 店铺等级-历史成交率
    test = pd.merge(test,shopstar_pcvr,'left',['shop_star_level'])
    return train,test



    features = [## 商品基本特征
                'item_id', 'item_brand_id','item_city_id', 
                'item_price_level', 'item_sales_level',
                'item_collected_level', 'item_pv_level', 
                'category_last'
                ## 用户基本特征
                'user_gender_id', 'user_occupation_id',
                'user_age_level', 'user_star_level', 
                ## 上下文基本特征
                'context_page_id', 
                ## 商铺相关特征
                'shop_review_num_level', 'shop_star_level','shop_id',
                'shop_review_positive_rate', 'shop_score_service', 'shop_score_delivery', 'shop_score_description',
                ## 用户点击特征
                'user_query_day', 'user_query_day_hour', 
                ## 时间相关特征
                'context_timestamp_Hour'
                ## 广告预测类别、属性的准确性
                'same_property_prob','predict_property_num','same_property_num',
                'same_category_prob','predict_category_num','same_category_num',
                 ## 历史cvr转换率特征
                'category_pcvr','category_C','category_I',
                #'brand_pcvr','brand_C','brand_I',
                'city_pcvr','city_C','city_I',
                'price_pcvr','price_C','price_I',
                'sales_pcvr','sales_C','sales_I',
                'collected_pcvr','collected_C','collected_I',
                'pv_pcvr','pv_C','pv_I',
                'gender_pcvr','gender_C','gender_I',
                'age_pcvr','age_C','age_I',
                'occupation_pcvr','occupation_C','occupation_I',
                'userstar_pcvr','userstar_C','userstar_I',
                'hour_pcvr','hour_C','hour_I',
                'page_pcvr','page_C','page_I',
                'shop_review_pcvr','shop_review_C','shop_review_I',
                'shopstar_pcvr','shopstar_C','shopstar_I',

                ## 4.2 编码后的类别特征,需要将编码前的特征注释掉
                ## 类别编码
                'item_brand_id_enc','item_city_id_enc','shop_id_enc','category_last_enc'
                ## 是否需要编码？？ 会默认把缺失值当作一类分类处理
                'shop_star_level_enc','context_page_enc','user_occupation_id_enc',
                'user_star_level_enc','user_age_level_enc',               
               ]



if __name__ == "__main__":
    print("读取数据\n")
    data = pd.read_table("../data/round1_ijcai_18_train_20180301.txt",delim_whitespace = True)
    test = pd.read_table("../data/round1_ijcai_18_test_a_20180301.txt",delim_whitespace = True)
    
    
    ## 对全部数据进行处理,先组合训练集和测试集
    print("对全部数据进行处理\n")
    data['source']='train'
    test['source']='test'
    data = pd.concat([data,test])
    data = prepare_data(data)
    train = data[data['source']=='train']
    test = data[data['source']=='test']
    
    ## 划分训练数据和测试数据
    print("划分训练数据和测试数据\n")
    #18,19,20,21,22,23,24   
    train = train.loc[(data.context_timestamp_day <= 24)&(train.context_timestamp_day >= 20)]  
    #test = data.loc[(data.context_timestamp_day == 24)]  #20,21,22,23,24   

    
    ## 对训练集进行特征构造   对测试集进行特征的填充
    print("对训练集进行特征构造   对测试集进行特征的填充\n")
    train,test = prepare_train_test(train,test)                                              #25线上数据

    ## 数据持久化
    print("将特征处理后的数据写入磁盘")
    train_handled = open('../data/sub_train_handled.pkl','wb')
    pickle.dump(train,train_handled)
    train_handled.close()
    
    test_handled = open('../data/sub_test_handled.pkl','wb')
    pickle.dump(test,test_handled)
    test_handled.close()
