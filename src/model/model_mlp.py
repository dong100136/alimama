import pandas as pd
import numpy as np
from scipy import sparse as ssp
from sklearn.model_selection import KFold
from keras.preprocessing.text import one_hot
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input, Embedding, LSTM, Dense,Flatten, Dropout, merge,Convolution1D,MaxPooling1D,Lambda
from keras.layers.advanced_activations import PReLU,LeakyReLU,ELU
from keras.models import Model
from keras.layers.normalization import BatchNormalization
import h5py
import os
from sklearn.preprocessing import StandardScaler
from keras.callbacks import EarlyStopping, ModelCheckpoint
import pickle
#读取特征工程后的数据
train_read = open('../data/train_train_handled.pkl','rb')
train = pickle.load(train_read)
test_read = open('../data/train_test_handled.pkl','rb')
test = pickle.load(test_read)

target = 'is_trade'
features = [## 商品基本特征
                'item_id', 'item_brand_id','item_city_id', 
                'item_price_level', 'item_sales_level',
                'item_collected_level', 'item_pv_level', 
                #'category_last',
                ## 用户基本特征
                'user_gender_id', 'user_occupation_id',
                'user_age_level', 'user_star_level', 
                ## 上下文基本特征
                'context_page_id', 
                ## 商铺相关特征
                'shop_review_num_level', 'shop_star_level','shop_id',
                'shop_review_positive_rate', 'shop_score_service', 'shop_score_delivery', 'shop_score_description',
                ## 用户点击特征
                'user_query_day', 'user_query_day_hour',
                ## 时间相关特征
                'context_timestamp_Hour',
                ## 广告预测类别、属性的准确性
                'same_property_prob','predict_property_num','same_property_num',
                'same_category_prob','predict_category_num','same_category_num',
                ## 4.2 编码后的类别特征
                ## 类别编码
                'item_brand_id_enc','item_city_id_enc','shop_id_enc','category_last_enc',
                ## 是否需要编码？？ 会默认把缺失值当作一类分类处理
                'shop_star_level_enc','context_page_enc','user_occupation_id_enc',
                'user_star_level_enc','user_age_level_enc',
                

                ## 历史cvr转换率特征
                'category_pcvr','category_C','category_I',
                #'brand_pcvr','brand_C','brand_I',
                'city_pcvr','city_C','city_I',
                'price_pcvr','price_C','price_I',
                'sales_pcvr','sales_C','sales_I',
                'collected_pcvr','collected_C','collected_I',
                'pv_pcvr','pv_C','pv_I',
                'gender_pcvr','gender_C','gender_I',
                'age_pcvr','age_C','age_I',
                'occupation_pcvr','occupation_C','occupation_I',
                'userstar_pcvr','userstar_C','userstar_I',
                'hour_pcvr','hour_C','hour_I',
                'page_pcvr','page_C','page_I',
                'shop_review_pcvr','shop_review_C','shop_review_I',
                'shopstar_pcvr','shopstar_C','shopstar_I',
               ]
X_train = train[features].values
y= train[target].values

X_test = test[features].values
y_test = test[target].values

from sklearn.preprocessing import StandardScaler,MinMaxScaler
st = MinMaxScaler()
st.fit(X_train)
X_train = st.transform(X_train)
X_test = st.transform(X_test)

def MLP(opt='nadam'):
    X_raw = Input(shape=(X_train.shape[1],), name='input_raw')

    fc1 = BatchNormalization()(X_raw)
    fc1 = Dense(512)(fc1)
    fc1 = PReLU()(fc1)
    fc1 = Dropout(0.25)(fc1)

    fc1 = BatchNormalization()(fc1)
    fc1 = Dense(256)(fc1)
    fc1 = PReLU()(fc1)
    fc1 = Dropout(0.15)(fc1)

    fc1 = BatchNormalization()(fc1)
    auxiliary_output_dense = Dense(1, activation='sigmoid', name='aux_output_dense')(fc1)

    output_all = Dense(1, activation='sigmoid', name='output')(fc1)
    model = Model(input=X_raw, output=output_all)
    model.compile(
        optimizer=opt,
        loss='binary_crossentropy')
    return model


model_mlp=MLP()
#model_name = 'mlp.hdf5'
#model_checkpoint = ModelCheckpoint(path+model_name, monitor='val_loss', save_best_only=True,mode='min')
model_mlp.fit(X_train,y,batch_size=1024,nb_epoch=14,verbose=2,
   validation_data=[X_train,y],shuffle=True)
res=model_mlp.predict(X_test,batch_size=1024)


from sklearn.metrics import log_loss
print ("logloss Score (Test): %f" % log_loss(test[target], res))