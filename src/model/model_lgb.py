import pandas as pd
import numpy as np 
from pandas import Series,DataFrame
from datetime import datetime
from sklearn.ensemble import RandomForestClassifier
import lightgbm as lgb
from sklearn.metrics import log_loss
import pickle
from sklearn import preprocessing
lbl = preprocessing.LabelEncoder()

""""
参数: 线上0.0826
    clf = lgb.LGBMClassifier(
        objective='binary',
        # metric='binary_error',
        num_leaves=31,
        depth=8,
        learning_rate=0.05,
        seed=2018,
        colsample_bytree=0.8,
        # min_child_samples=8,
        subsample=0.9,
        n_estimators=307)


""""


if __name__ == "__main__":

    ## 列出训练特征
    features = ['item_id', 'item_brand_id', 'item_city_id', 'item_price_level', 'item_sales_level',
                'item_collected_level', 'item_pv_level', 'user_gender_id', 'user_occupation_id',
                'user_age_level', 'user_star_level', 'context_page_id', 'shop_id', 'shop_review_num_level', 'shop_star_level',
                'shop_review_positive_rate', 'shop_score_service', 'shop_score_delivery', 'shop_score_description',
                'category_last','context_timestamp_Hour','sales_pcvr',
                'user_query_day', 'user_query_day_hour',
                'same_property_num','same_category_num',
                


                #'category_pcvr','category_C','category_I',
                #'brand_pcvr','brand_C','brand_I',
                'city_pcvr','city_C','city_I',
                'price_pcvr','price_C','price_I',
                'sales_pcvr','sales_C','sales_I',
                'collected_pcvr','collected_C','collected_I',
                'pv_pcvr','pv_C','pv_I',
                'gender_pcvr','gender_C','gender_I',
                'age_pcvr','age_C','age_I',
                'occupation_pcvr','occupation_C','occupation_I',
                'userstar_pcvr','userstar_C','userstar_I',
                'hour_pcvr','hour_C','hour_I',
                #'page_pcvr','page_C','page_I',
                'shop_review_pcvr','shop_review_C','shop_review_I',
                'shopstar_pcvr','shopstar_C','shopstar_I',
               ]

    ## 离线数据训练模型
    best_iter = offline_lgb_model(features)
    ## 预测在线数据
    online_lgb_model(feature)








def offline_lgb_model(features):
    print("读取数据\n")
    data = pd.read_table("../data/round1_ijcai_18_train_20180301.txt",delim_whitespace = True)
    test = pd.read_table("../data/round1_ijcai_18_test_a_20180301.txt",delim_whitespace = True)
    
    ## 对全部数据进行处理
    print("对全部数据进行处理\n")
    data = prepare_data(data)
    test = prepare_data(test)
    
    ## 划分训练数据和测试数据
    print("划分训练数据和测试数据\n")
    train = data.loc[(data.context_timestamp_day <= 24)&(data.context_timestamp_day >= 20)]  # 18,19,20,21,22,23,24   25线上数据
    
    ## 对训练集进行特征构造   对测试集进行特征的填充
    print("对训练集进行特征构造   对测试集进行特征的填充\n")
    train,test = prepare_train_test(train,test)

    ## 目标项
    target = ['is_trade']

    ## 训练lgb模型
    X = train[feature]
    y = train['is_trade'].values
    X_tes = test[feature]
    y_tes = test['is_trade'].values
    print('Training LGBM model...')
    lgb = lgb.LGBMClassifier(
        objective='binary',
        # metric='binary_error',
        num_leaves=31,
        depth=8,
        learning_rate=0.05,
        seed=2018,
        colsample_bytree=0.8,
        # min_child_samples=8,
        subsample=0.9,
        n_estimators=20000)
    lgb_model = lgb.fit(X, y, eval_set=[(X_tes, y_tes)], early_stopping_rounds=200)
    best_iter = lgb_model.best_iteration_

    
    predictors = [i for i in X.columns]
    feat_imp = pd.Series(lgb_model.feature_importance(), predictors).sort_values(ascending=False)
    print(feat_imp)
    print(feat_imp.shape)
    pred = lgb_model.predict_proba(test[feature])[:, 1]
    print('误差 ', log_loss(test['is_trade'], test['pred']))
    return best_iter


def online_lgb_model(feature):
    print("读取数据\n")
    data = pd.read_table("../data/round1_ijcai_18_train_20180301.txt",delim_whitespace = True)
    test = pd.read_table("../data/round1_ijcai_18_test_a_20180301.txt",delim_whitespace = True)
    
    ## 对全部数据进行处理
    print("对全部数据进行处理\n")
    data = prepare_data(data)
    
    ## 划分训练数据和测试数据
    print("划分训练数据和测试数据\n")
    train = data.loc[(data.context_timestamp_day <= 24)&(data.context_timestamp_day >= 20)]  # 18,19,20,21,22,23,24   25线上数据
    
    ## 对训练集进行特征构造   对测试集进行特征的填充
    print("对训练集进行特征构造   对测试集进行特征的填充\n")
    train,test = prepare_train_test(train,test)

    ## 目标项
    target = ['is_trade']

    ## 训练lgb模型
    print("训练lgb模型\n")
    clf = lgb.LGBMClassifier(
        objective='binary',
        # metric='binary_error',
        num_leaves=31,
        depth=8,
        learning_rate=0.05,
        seed=2018,
        colsample_bytree=0.8,
        # min_child_samples=8,
        subsample=0.9,
        n_estimators=307)
    clf.fit(train[features], train[target],  feature_name=features,categorical_feature=['user_gender_id','category_last', 'item_brand_id',])
    test['predicted_score'] = clf.predict_proba(test[features],)[:, 1]
    test[['instance_id', 'predicted_score']].to_csv('../result/lgb_x_xx.txt', index=False,sep=' ')#保存在线提交结果