
cate_features = ['context_page_id','item_brand_id',
                'item_city_id','item_collected_level',
                'item_id','item_price_level',
                'item_pv_level','item_sales_level',
                'shop_id','shop_review_num_level',
                'shop_star_level','user_age_level',
                'user_gender_id','user_occupation_id',
                'user_star_level','item_property_list',
                'item_category_list','context_hour']




def create_time_feature(df):
    """
    构建时间特征 日 时 
    """
    df['context_timestamp'] = df.apply(lambda x : x.context_timestamp+28800,axis=1)
    df['context_timestamp_datetype'] = pd.to_datetime(df['context_timestamp'],unit='s')
    df['context_day'] = [datetime.strftime(x,'%d') for x in df['context_timestamp_datetype']]
    df['context_hour'] = [datetime.strftime(x,'%H') for x in df['context_timestamp_datetype']]
    df['context_day'] = df['context_day'].astype('int')
    df['context_hour'] = df['context_hour'].astype('int')
    return df


def split_category(x,index):
    tmp  = []
    for a in x:
        tmp.append(a.split(':',1)[index])
    return tmp  

def split_property(x):
    tmp  = []
    for a in x:
        tmp.extend(a.split(',',a.count(",")))
    return tmp 

def check_same(a,b):
    set_same = a & b
    return list(set_same)

def create_similarity_feature(data):
    """
    预测的类别和属性和广告商品的相似度特征
    """
    data['predict_category_list'] = data['predict_category_property'].apply(lambda x:x.split(';',x.count(';')))
    data['predict_category'] = data['predict_category_list'].apply(lambda x:split_category(x,0))
    data['predict_category'] = data['predict_category'].apply(lambda x:set(x))
    ## 切分广告商品类别特征
    data['item_category'] = data['item_category_list'].apply(lambda x:x.split(';',x.count(';')))
    data['item_category'] = data['item_category'].apply(lambda x:set(x))
    ## 筛出商品类别和预测类别的交集
    data['same_category_list'] = list(map(check_same,data['predict_category'], data['item_category']))
    ## 计算预测类别和广告商品类别的相似程度
    data['same_category_num'] = data['same_category_list'].apply(lambda x:len(x))
    data['predict_category_num'] = data['predict_category'].apply(lambda x:len(x))
    data['same_category_prob'] = data['same_category_num']/data['predict_category_num']

    ## 逐步切分预测属性特征
    data['predict_property'] = data['predict_category_list'].apply(lambda x:split_category(x,-1))
    data['predict_property'] = data['predict_property'].apply(lambda x:split_property(x))
    data['predict_property']= data['predict_property'].apply(lambda x:set(x))
    ## 切分广告商品属性特征
    data['item_property'] = data['item_property_list'].apply(lambda x:x.split(';',x.count(';')))
    data['item_property'] = data['item_property'].apply(lambda x:set(x))
    ## 筛出商品属类别和预测类别的交集
    data['same_property_list'] = list(map(check_same,data['predict_property'], data['item_property']))
    ## 计算预测属性和广告商品属性的相似程度
    data['same_property_num'] = data['same_property_list'].apply(lambda x:len(x))
    data['predict_property_num'] = data['predict_property'].apply(lambda x:len(x))
    data['same_property_prob'] = data['same_property_num']/data['predict_property_num']
    return data


def create_trick_feature(data):
    """
    添加trick特征，标记该条点击是不是第一次点击或者最后一次点击，
    最后一次点击的购买概率会很大，之前的都会很小
    """
    trick_user_item_cnt = data.groupby(['user_id','item_id',]).size().reset_index().rename(columns={0: 'trick_user_item_cnt'})
    data = pd.merge(data, trick_user_item_cnt, 'left', on=['user_id', 'item_id'])
    data['is_repeat_click'] = data['trick_user_item_cnt'].apply(lambda x:1 if x>1 else 0)
    repeat_data=data.loc[data['is_repeat_click']==1]
    repeat_data=repeat_data.sort_values(by=['context_timestamp']) 
    first_click = repeat_data.drop_duplicates(subset=['user_id','item_id'],keep='first')
    first_click['is_first_click']=1
    last_click = repeat_data.drop_duplicates(subset=['user_id','item_id'],keep='last')
    last_click['is_last_click']=1
    first_click=first_click.loc[:,['instance_id','is_first_click']]
    data = pd.merge(data, first_click, 'left', on=['instance_id'])
    last_click=first_click.loc[:,['instance_id','is_last_click']]
    data = pd.merge(data, last_click, 'left', on=['instance_id'])
    data['is_first_click'] = data['is_first_click'].apply(lambda x:1 if x==1 else 0)
    data['is_last_click'] = data['is_last_click'].apply(lambda x:1 if x==1 else 0)
    return data

def create_query_data(data):
    """
    添加点击统计特征，该商品、商铺、用户一天和一小时的点击数量
    """
    user_query_day = data.groupby(['user_id', 'context_day']).size(
    ).reset_index().rename(columns={0: 'user_query_day'})
    data = pd.merge(data, user_query_day, 'left', on=['user_id', 'context_day'])
    user_query_day_hour = data.groupby(['user_id', 'context_day', 'context_hour']).size().reset_index().rename(
        columns={0: 'user_query_day_hour'})
    data = pd.merge(data, user_query_day_hour, 'left',
                    on=['user_id', 'context_day', 'context_hour'])

    shop_query_day=data.groupby(['context_day','shop_id']).size().reset_index().rename(columns={0: 'shop_query_day'})
    data = pd.merge(data, shop_query_day, 'left', on=['shop_id', 'context_day'])
    shop_query_day_hour=data.groupby(['context_day','context_hour','shop_id']).size().reset_index().rename(columns={0: 'shop_query_day_hour'})
    data = pd.merge(data, shop_query_day_hour, 'left', on=['shop_id', 'context_day','context_hour'])
    
    item_query_day=data.groupby(['context_day','item_id']).size().reset_index().rename(columns={0: 'item_query_day'})
    data = pd.merge(data, item_query_day, 'left', on=['item_id', 'context_day'])
    item_query_day_hour=data.groupby(['context_day','context_hour','item_id']).size().reset_index().rename(columns={0: 'item_query_day_hour'})
    data = pd.merge(data, item_query_day_hour, 'left', on=['item_id', 'context_day','context_hour'])
    return data  

def create_category_feature(data,cate_features):
    for i,feat in enumerate(cate_features):
        data[feat] = lbl.fit_transform(data[feat])



def create_shop_item_feature(data):
    ##  一个shop有多少item打广告
    tmp = data.groupby(['shop_id','item_id'], as_index=False)['instance_id'].agg({'item_cnt': 'count'})
    tmp['item_cnt']=1
    tmp=tmp.groupby(['shop_id'], as_index=False)['shop_id'].agg({'shop_item_cnt': 'count'})
    data=pd.merge(data,tmp,on=['shop_id'],how='left')
    ##  一个shop有多少property
    tmp = data.groupby(['shop_id','item_property_list'], as_index=False)['instance_id'].agg({'item_cnt': 'count'})
    tmp['item_cnt']=1
    tmp=tmp.groupby(['shop_id'], as_index=False)['shop_id'].agg({'shop_property_cnt': 'count'})
    data=pd.merge(data,tmp,on=['shop_id'],how='left')
    ##  一个shop有多少category
    tmp = data.groupby(['shop_id','item_category_list'], as_index=False)['instance_id'].agg({'item_cnt': 'count'})
    tmp['item_cnt']=1
    tmp=tmp.groupby(['shop_id'], as_index=False)['shop_id'].agg({'shop_category_cnt': 'count'})
    data=pd.merge(data,tmp,on=['shop_id'],how='left')