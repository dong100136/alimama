import sys
sys.path.append('./features')

import pandas as pd
import numpy as np 
from pandas import Series,DataFrame
from datetime import datetime
import lightgbm as lgb
from sklearn.metrics import log_loss
from sklearn import preprocessing

from combineData import get_final_data
import utils

class ModelLGB(object):
    def __init__(self):
        self.model = lgb.LGBMClassifier(
			objective='binary',
			metric='binary_logloss',
			num_leaves=31,
			max_depth=-1,
			learning_rate=0.1,
			n_estimators=400,
			class_weight='balanced',
			subsample=0.7,
			colsample_bytree=0.7,
			reg_alpha=1,
			reg_lambda=0.01,
			random_state=2008,
			n_jobs=-1,
			silent=False
        )

    def fit(self,X,y):
        self.model.fit(X,y)

    def predict(self,data):
        y_predict = self.model.predict_proba(data)[:,1]
        return y_predict


if __name__ == "__main__":

    ## 列出训练特征
    features = []
    # 广告相关的特征
    features.extend(['item_id', 'item_brand_id', 'item_city_id', 'item_price_level', 'item_sales_level',
                'item_collected_level', 'item_pv_level', 'item_category_0','item_category_1','item_category_2',])
    # 用户相关的特征
    features.extend(['user_gender_id', 'user_occupation_id','user_age_level', 'user_star_level',])
    # 上下文相关的特征
    features.extend(['context_page_id','context_timestamp_Hour','context_timestamp_weekday'])
    # 店铺相关的特征
    features.extend(['shop_id', 'shop_review_num_level', 'shop_star_level',
                'shop_review_positive_rate', 'shop_score_service', 'shop_score_delivery', 'shop_score_description',])
    # pcvr特征
    features.extend([
                'item_category_1_pcvr','item_category_1_C','item_category_1_I',
                'item_category_2_pcvr','item_category_2_C','item_category_2_I',
                'item_brand_id_pcvr','item_brand_id_C','item_brand_id_I',
                'item_city_id_pcvr','item_city_id_C','item_city_id_I',
                'item_price_level_pcvr','item_price_level_C','item_price_level_I',
                'item_sales_level_pcvr','item_sales_level_C','item_sales_level_I',
                'item_collected_level_pcvr','item_collected_level_C','item_collected_level_I',
                'item_pv_level_pcvr','item_pv_level_C','item_pv_level_I',
                'user_gender_id_pcvr','user_gender_id_C','user_gender_id_I',
                'user_age_level_pcvr','user_age_level_C','user_age_level_I',
                'user_occupation_id_pcvr','user_occupation_id_C','user_occupation_id_I',
                'user_star_level_pcvr','user_star_level_C','user_star_level_I',
                'context_timestamp_Hour_pcvr','context_timestamp_Hour_C','context_timestamp_Hour_I',
                'context_page_id_pcvr','context_page_id_C','context_page_id_I',
                'shop_review_num_level_pcvr','shop_review_num_level_C','shop_review_num_level_I',
                'shop_star_level_pcvr','shop_star_level_C','shop_star_level_I'])

    features+=utils.load_pickle(utils.temp_data_path+"brand_feature_name.plk")
    features+=utils.load_pickle(utils.temp_data_path+"shop_feature_name.plk")
    features+=utils.load_pickle(utils.temp_data_path+"item_feature_name.plk")
    features+=utils.load_pickle(utils.temp_data_path+"category_feature_name.plk")
    features+=utils.load_pickle(utils.temp_data_path+"user_feature_name.plk")

    features = [x for x in features if x.find('0d')==-1]
	# features = [x for x in features if x.find('1d')==-1]
	# features = [x for x in features if x.find('2d')==-1]
	# features = [x for x in features if x.find('3d')==-1]
	# features = [x for x in features if x.find('_I')==-1]
	# features = [x for x in features if x.find('_C')==-1]
    features = [x for x in features if x.find('_is_trade')==-1]
 
    data = get_final_data()
    data = data[data['context_timestamp_day']!=18]
    data = data[data['context_timestamp_day']!=19]
    data = data[data['context_timestamp_day']!=20]

    print("共使用了%d个特征" % len(features))
    utils.local_test(data,features,ModelLGB)

    train,test = utils.get_online_train_test(data)
    model = ModelLGB()
    model.fit(train[features],train['is_trade'])
    test['predicted_score'] = model.predict(test[features])
    test[['instance_id','predicted_score']].to_csv(utils.result_out_path,sep=" ",index=False)

    '''
    本地测试：0.081202
    线上： 

    本地： 0.010981 ~0.0155
    '''
