'''
## 用户相关的特征

- 同龄人下一天，两天，三天的展示次数，点击次数，点击率
- 同职业下一天，两天，三天的展示次数，点击次数，点击率
- 同等级下一天，两天，三天的展示次数，点击次数，点击率
- 同性别下一天，两天，三天的展示次数，点击次数，点击率

- 该用户是否点击过该种类
- 该用户是否点击过该店铺
- 该用户是否点击过该品牌
- 该用户是否点击过该商品
'''

import utils
from tqdm import tqdm
import preprocess1 as pre
import pandas as pd

def get_cache_user_features():
    data = utils.load_pickle(utils.temp_data_path+"user_feature.plk")
    return data

def is_buy_before(data,col1,col2):
    count_name = col1+'_'+col2+"_is_trade"
    t = data.groupby([col1,col2])['is_trade'].sum().reset_index()
    t.fillna(0,inplace=True)
    t.columns = [col1,col2,count_name]
    data = pd.merge(data,t,'left',[col1,col2])
    return data,[count_name]

def gen_buy_before(data):
    features = [
        ['user_id','shop_id'],
        ['user_id','item_category_1'],
        ['user_id','item_brand_id'],
        ['user_id','item_id']
    ]

    feature_names = []

    for feature in tqdm(features):
        data,feature_name = is_buy_before(data,feature[0],feature[1])
        feature_names = feature_names+feature_name
    
    return data,feature_names

def gen_pcvr_feature(data):
    features = [
        [['user_gender_id'],[0,1,2,3]],
        [['user_age_level'],[0,1,2,3]],
        [['user_occupation_id'],[0,1,2,3]],
        [['user_star_level'],[0,1,2,3]]
    ]

    feature_names=[]
    
    for feature in tqdm(features):
        data,feature_name= utils.calc_pcvr(data,feature[0],feature[1])
        feature_names = feature_names+feature_name
    
    return data,feature_names

def gen_features(data):
    feature_names = []
    data,feature_name = gen_buy_before(data)
    feature_names = feature_names+feature_name
    data,feature_name = gen_pcvr_feature(data)
    feature_names = feature_names+feature_name

    return data,feature_names

if __name__=="__main__":
    data = pre.get_all_data()

    data,new_feature_names = gen_features(data)

    utils.dump_pickle(new_feature_names,utils.temp_data_path+"user_feature_name.plk")

    new_feature_names = ['global_index']+new_feature_names

    print("生成了下面的新特征:\n %s" % '\n'.join(new_feature_names))

    utils.dump_pickle(data[new_feature_names],utils.temp_data_path+"user_feature.plk")
    utils.dump_pickle(True,utils.temp_data_path+"user_feature.update.plk")