'''
## 类别相关的特征（直接使用最后一个特征）

- 该种类下一天，两天，三天的展示次数，点击次数，点击率

- 该种类下品牌数
- 该种类下店铺数
- 该种类下产品数

- 同龄人在该种类的展示次数，点击次数，点击率
- 同职业在该种类的展示次数，点击次数，点击率
- 同等级在该种类的展示次数，点击次数，点击率
- 同性别在该种类的展示次数，点击次数，点击率

'''

import utils
from tqdm import tqdm
import preprocess1 as pre
import pandas as pd

def get_cache_category_features():
    data = utils.load_pickle(utils.temp_data_path+"category_feature.plk")
    return data

def gen_count_features(data):
    features = [
        ['item_category_1','item_brand_id'],
        ['item_category_1','shop_id'],
        ['item_category_1','item_id']
    ]

    feature_names = []

    for v in tqdm(features):
        data,f = utils.calc_count(data,v[0],v[1])
        feature_names = feature_names+f

    return data,feature_names

def gen_pcvr_features(data):
    features = [
        [['item_category_1'],[0,1,2,3]],
        [['user_gender_id','item_category_1'],[0,1,2,3]],
        [['user_age_level','item_category_1'],[0,1,2,3]],
        [['user_occupation_id','item_category_1'],[0,1,2,3]],
        [['user_star_level','item_category_1'],[0,1,2,3]]
    ]

    feature_names = []

    for v in tqdm(features):
        data,f = utils.calc_pcvr(data,v[0],v[1])
        feature_names = feature_names+ f

    return data,feature_names

def gen_features(data):
    feature_names = []

    data,f = gen_count_features(data)
    feature_names = feature_names+f

    data,f = gen_pcvr_features(data)
    feature_names = feature_names+f

    return data,feature_names
    

if __name__=='__main__':
    data = pre.get_all_data()

    data,new_feature_names = gen_features(data)

    utils.dump_pickle(new_feature_names,utils.temp_data_path+"category_feature_name.plk")

    new_feature_names = ['global_index']+new_feature_names

    print("生成了下面的新特征:\n %s" % '\n'.join(new_feature_names))

    utils.dump_pickle(data[new_feature_names],utils.temp_data_path+"category_feature.plk")
    utils.dump_pickle(True,utils.temp_data_path+"category_feature.update.plk")
