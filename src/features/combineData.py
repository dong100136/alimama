
from preprocess1 import get_all_data, preprocess
from pcvr2 import get_cache_pcvr
from userFeatures import get_cache_user_features
from brandFeatures import get_cache_brand_features
from itemFeatures import get_cache_item_features
from categoryFeatures import get_cache_category_features
from shopFeatures import get_cache_shop_features

import utils
import pandas as pd


def get_final_data():
    pcvr_update = utils.load_pickle(utils.temp_data_path+"pcvr.update.plk")
    user_update = utils.load_pickle(
        utils.temp_data_path+"user_feature.update.plk")
    brand_update = utils.load_pickle(
        utils.temp_data_path+"brand_feature.update.plk")
    item_update = utils.load_pickle(
        utils.temp_data_path+"item_feature.update.plk")
    category_update = utils.load_pickle(
        utils.temp_data_path+"category_feature.update.plk")
    shop_update = utils.load_pickle(
        utils.temp_data_path+"shop_feature.update.plk")

    if pcvr_update or user_update or brand_update or item_update or category_update or shop_update:
        print("数据有更新，重新生成数据中.......")
        create_final_data()
        utils.dump_pickle(False, utils.temp_data_path+"pcvr.update.plk")
        utils.dump_pickle(False, utils.temp_data_path +
                          "user_feature.update.plk")
        utils.dump_pickle(False, utils.temp_data_path +
                          "brand_feature.update.plk")
        utils.dump_pickle(False, utils.temp_data_path +
                          "item_feature.update.plk")
        utils.dump_pickle(False, utils.temp_data_path +
                          "category_feature.update.plk")
        utils.dump_pickle(False, utils.temp_data_path +
                          "shop_feature.update.plk")
    else:
        print("使用缓存数据...")

    data = utils.load_pickle(utils.temp_data_path+"final_data.plk")
    print('共用%d个特征' % len(data.columns))

    return data


def create_final_data():
    data = get_all_data()
    pcvr = get_cache_pcvr()
    user_features = get_cache_user_features()
    brand_faetures = get_cache_brand_features()
    item_features = get_cache_item_features()
    category_feature = get_cache_category_features()
    shop_feature = get_cache_shop_features()

    all_data = pd.merge(data, pcvr, 'left', ['global_index'])
    all_data = pd.merge(all_data, user_features, 'left', ['global_index'])
    all_data = pd.merge(all_data, brand_faetures, 'left', ['global_index'])
    all_data = pd.merge(all_data, item_features, 'left', ['global_index'])
    all_data = pd.merge(all_data, category_feature, 'left', ['global_index'])
    all_data = pd.merge(all_data, shop_feature, 'left', ['global_index'])

    utils.dump_pickle(all_data, utils.temp_data_path+"final_data.plk")

    feature_names = []
    file_names = ['brand_feature_name.plk', 'category_feature_name.plk',
                  'item_feature_name.plk', 'shop_feature_name.plk', 'user_feature_name.plk']
    for file_name in file_names:
        feature_names += utils.load_pickle(utils.temp_data_path+file_name)
    utils.dump_pickle(feature_names, utils.temp_data_path +
                      "final_data_feature_name.plk")


if __name__ == '__main__':
    create_final_data()
