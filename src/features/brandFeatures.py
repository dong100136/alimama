'''
品牌相关的特征
- 该品牌一天，两天，三天前，总的展示次数
- 该品牌一天，两天，三天前，总的点击次数
- 该品牌一天，两天，三天前，总的点击率

- 品牌相关的店铺数
- 品牌相关的种类数
- 品牌相关的产品数

- 同龄人在该品牌的展示次数，点击次数，点击率
- 同职业在该品牌的展示次数，点击次数，点击率
- 同等级在该品牌的展示次数，点击次数，点击率
- 同性别在该品牌的展示次数，点击次数，点击率
'''

import utils
from tqdm import tqdm
import preprocess1 as pre
import pandas as pd

def get_cache_brand_features():
    data = utils.load_pickle(utils.temp_data_path+"brand_feature.plk")
    return data

def gen_count_feature(data):
    features = [
        ['item_brand_id','shop_id'],
        ['item_brand_id','item_category_1'],
        ['item_brand_id','item_id']
    ]

    features_names = []

    for feature in tqdm(features):
        data,feature_name= utils.calc_count(data,feature[0],feature[1])
        features_names = features_names+feature_name
    return data,features_names


def gen_pcvr_feature(data):
    features = [
            [['item_brand_id'],[0,1,2,3]],
            [['user_gender_id','item_brand_id'],[0,1,2,3]],
            [['user_age_level','item_brand_id'],[0,1,2,3]],
            [['user_occupation_id','item_brand_id'],[0,1,2,3]],
            [['user_star_level','item_brand_id'],[0,1,2,3]]
        ]

    feature_names = []

    for feature in tqdm(features):
        data,feature_name= utils.calc_pcvr(data,feature[0],feature[1])
        feature_names = feature_names+feature_name
    
    return data,feature_names

def gen_features(data):
    feature_names = []
    data,feature_name = gen_count_feature(data)
    feature_names = feature_names+feature_name
    data,feature_name = gen_pcvr_feature(data)
    feature_names = feature_names+feature_name

    return data,feature_names
    

if __name__=='__main__':
    data = pre.get_all_data()

    data,new_feature_names = gen_features(data)

    utils.dump_pickle(new_feature_names,utils.temp_data_path+"brand_feature_name.plk")

    new_feature_names = ['global_index']+new_feature_names

    print("生成了下面的新特征:\n %s" % '\n'.join(new_feature_names))

    utils.dump_pickle(data[new_feature_names],utils.temp_data_path+"brand_feature.plk")
    utils.dump_pickle(True,utils.temp_data_path+"brand_feature.update.plk")
