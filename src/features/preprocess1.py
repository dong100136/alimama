import numpy as np
import pandas as pd
import os

import utils
import sklearn.preprocessing as pcs


def get_all_data():
    all_data_pkl = utils.temp_data_path+"all_data.plk"
    if (os.path.exists(all_data_pkl)):
        data = utils.load_pickle(all_data_pkl)
    else:
        train = read_train_data()
        test = read_test_data()
        train['type'] = 'train'
        test['type'] = 'test'
        data = pd.concat([train, test])
        data['global_index'] = np.arange(0, data.shape[0])

        data = preprocess(data)
        data.loc[data['context_timestamp_day'] == 24, 'type'] = 'valid'

        y = data.loc[data['type'] == 'valid', ['instance_id', 'is_trade']]
        utils.dump_pickle(y, utils.temp_data_path+"data.y.plk")

        # 删除验证集的is_trade
        data.loc[data['type'] == 'valid', 'is_trade'] = np.NaN

        utils.dump_pickle(data, all_data_pkl)
    return data


def read_train_data():
    train_data = utils.temp_data_path+"train_data.plk"
    if (os.path.exists(train_data)):
        data = utils.load_pickle(train_data)
    else:
        data = pd.read_csv(utils.origin_train_data_path, sep=" ")
        utils.dump_pickle(data, train_data)
    return data


def read_test_data():
    test_data = utils.temp_data_path+"test_data.plk"
    if (os.path.exists(test_data)):
        data = utils.load_pickle(test_data)
    else:
        data = pd.read_csv(utils.origin_test_data_path, sep=" ")
        utils.dump_pickle(data, test_data)
    return data


def preprocess_context_timestamp(data):
    data['context_timestamp'] = pd.to_datetime(
        data['context_timestamp']+28800, unit='s')
    data['context_timestamp_Hour'] = data['context_timestamp'].map(
        lambda x: x.hour)
    data['context_timestamp_weekday'] = data['context_timestamp'].map(
        lambda x: x.weekday())
    data['context_timestamp_day'] = data['context_timestamp'].map(
        lambda x: x.day)
    return data


def preprocess_category(data):
    def get_category_list_func(pos):
        def get_category_list(x):
            arr = x.split(";")
            if len(arr) > pos:
                return arr[pos]
            else:
                return "-1"
        return get_category_list

    data['item_category_0'] = data['item_category_list'].map(
        get_category_list_func(0))
    data['item_category_1'] = data['item_category_list'].map(
        get_category_list_func(1))
    data['item_category_2'] = data['item_category_list'].map(
        get_category_list_func(2))
    data.drop("item_category_list", 1)

    data['item_category_0'] = pcs.LabelEncoder(
    ).fit_transform(data['item_category_0'])
    data['item_category_1'] = pcs.LabelEncoder(
    ).fit_transform(data['item_category_1'])
    data['item_category_2'] = pcs.LabelEncoder(
    ).fit_transform(data['item_category_2'])
    return data


def preprocess_property(data):
    return data


def preprocess(data):
    '''
    处理时间，类别信息
    '''
    data = preprocess_context_timestamp(data)
    data = preprocess_category(data)
    data = preprocess_property(data)

    return data


if __name__ == '__main__':
    data = get_all_data()
