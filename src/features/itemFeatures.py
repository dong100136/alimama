'''
## 商品相关的特征

- 商品的属性个数

- 是否为该种类下展示的前三种商品
- 是否为该种类下点击的前三种商品

- 是否为该品牌下展示最多的前三商品
- 是否为该品牌下点击最多的前三种商品

- 是否为该店铺下展示最多的前三种商品
- 是否为该店铺下点击最多的前三种商品

- 同龄人下一天，两天，三天的展示次数，点击次数，点击率
- 同职业下一天，两天，三天的展示次数，点击次数，点击率
- 同等级下一天，两天，三天的展示次数，点击次数，点击率
- 同性别下一天，两天，三天的展示次数，点击次数，点击率

- 销售该产品的店铺数
'''

import utils
from tqdm import tqdm
import preprocess1 as pre
import pandas as pd

def get_cache_item_features():
    data = utils.load_pickle(utils.temp_data_path+"item_feature.plk")
    return data

def calc_property_count(data):
    feature_name = "item_property_count"
    data['item_property_count'] = data['item_property_list'].map(lambda x: len(x.split(';')))
    return data,[feature_name]

def gen_count_features(data):
    data,f = utils.calc_count(data,'item_id','shop_id')
    return data,f

def calc_is_top_trade(data,col1,col2):
    feature_sum_name = col1+'_'+col2+"_view_is_max"
    feature_count_name = col1+'_'+col2+"count_is_max"
    a = data.groupby([col1,col2])['is_trade'].agg([sum,len]).reset_index()
    a[feature_sum_name] = a.groupby(col1)['sum'].transform(max)==a['sum']
    a[feature_count_name] = a.groupby(col1)['len'].transform(max)==a['len']
    data = pd.merge(data,a[[col1,col2,feature_count_name,feature_sum_name]],'left',[col1,col2])
    return data,[feature_count_name,feature_sum_name]

def gen_is_top_trade_features(data):
    features = [
        ['item_category_1','item_id'],
        ['item_brand_id','item_id'],
        ['shop_id','item_id']
    ]

    feature_names = []

    for v in tqdm(features):
        data,f = calc_is_top_trade(data,v[0],v[1])
        feature_names  = feature_names + f

    return data,feature_names

def gen_pcvr_features(data):
    features = [
        [['item_id'],[0,1,2,3]],
        [['user_gender_id','item_id'],[0,1,2,3]],
        [['user_age_level','item_id'],[0,1,2,3]],
        [['user_occupation_id','item_id'],[0,1,2,3]],
        [['user_star_level','item_id'],[0,1,2,3]]
    ]

    feature_names = []

    for v in tqdm(features):
        data,f = utils.calc_pcvr(data,v[0],v[1])
        feature_names = feature_names+ f

    return data,feature_names

def gen_features(data):
    feature_names = []

    data,f = gen_count_features(data)
    feature_names = feature_names+f

    data,f = gen_is_top_trade_features(data)
    feature_names = feature_names+f

    data,f = calc_property_count(data)
    feature_names = feature_names+f

    data,f = gen_pcvr_features(data)
    feature_names = feature_names+f

    return data,feature_names
    

if __name__=='__main__':
    data = pre.get_all_data()

    data,new_feature_names = gen_features(data)

    utils.dump_pickle(new_feature_names,utils.temp_data_path+"item_feature_name.plk")

    new_feature_names = ['global_index']+new_feature_names

    print("生成了下面的新特征:\n %s" % '\n'.join(new_feature_names))

    utils.dump_pickle(data[new_feature_names],utils.temp_data_path+"item_feature.plk")
    utils.dump_pickle(True,utils.temp_data_path+"item_feature.update.plk")
