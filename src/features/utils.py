import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss
import pandas as pd

from pcvr2 import BayesianSmoothing

base_path = '/home/yejiandong/tianchi/alimama/'
#base_path = "/Users/stone/Project/天池/IJCAI-18 阿里妈妈搜索广告转化预测/"

raw_data_path = base_path + "data/origin/"
origin_train_data_path = raw_data_path + "round1_ijcai_18_train_20180301.txt"
origin_test_data_path = raw_data_path + "round1_ijcai_18_test_a_20180301.txt"

temp_data_path = base_path+"data/temp/"
origin_train_data_pkl_path = temp_data_path+"train_data.pkl"
origin_test_data_pkl_path = temp_data_path+"test_data.pkl"

result_out_path = base_path+"data/out/rs.csv"


def load_pickle(path):
    return pickle.load(open(path, 'rb'))


def dump_pickle(obj, path, protocol=None,):
    pickle.dump(obj, open(path, 'wb'), protocol=protocol)


def get_local_train_test(data, type="date"):
    # train_data = data[data['type']=='train']
#    if type == 'date':
        # train = train_data['context_timestamp'].map(lambda x: x.day)<24
        # X_train = train_data[train]
        # X_test = train_data[~train]

    X_train = data[data['type'] == 'train']
    X_test = data[data['type'] == 'valid']
#    else:
#        X_train, X_test = train_test_split(data[data['type'] == 'train'])
    return X_train, X_test


def get_online_train_test(data):
    train = data[data['type'] == 'train']
    test = data[data['type'] == 'test']
    return train, test


def data_resample(data):
    data_pos = data[data['is_trade'] == 1]
    data_neg = data[data['is_trade'] == 0]

    size_of_neg = len(data_neg)

    data_pos = data_pos.sample(size_of_neg, replace=True)

    new_data = pd.concat([data_pos, data_neg])
    return new_data


def score(Y):
    '''
    放入的是numpy array对象
    '''
    y = load_pickle(temp_data_path+"data.y.plk")
    rs = pd.merge(y, Y[['instance_id', 'predicted_score']],
                  'left', 'instance_id')
    return log_loss(rs['is_trade'], rs['predicted_score'])


def local_test(data, features, Model):
    train, test = get_local_train_test(data)
    model = Model()
    model.fit(train[features], train['is_trade'])
    test['predicted_score'] = model.predict(test[features])
    score = log_loss(test['is_trade'], test['predicted_score'])
    print("按时间分 score: %.6f" % score)

    train, test = get_local_train_test(data, 'random')
    model = Model()
    model.fit(train[features], train['is_trade'])
    test['predicted_score'] = model.predict(test[features])
    score = log_loss(test['is_trade'], test['predicted_score'])
    print("随机分 score: %.6f" % score)

# def export_result(model):
#     data_test = read_test_data()
#     data_id = data_test['instance_id']
#     data_test = preproccess(data_test)
#     Y_predict = model.predict_proba(data_test)
#     rs = pd.DataFrame({"instance_id":data_id.values,"predicted_score":Y_predict[:,0]})
#     rs.to_csv(result_out_path,sep=" ",index=False)


def split_train_test(data):
    train = data[data['type'] == 'train']
    test = data[data['type'] == 'test']
    return train, test


def calc_pcvr(data, cols, before_n_day_list):
    '''
    计算展示次数，点击次数，点击率
    没有做平滑处理
    '''
    cols.append("is_trade")
    days = data['context_timestamp_day'].value_counts().index.values

    gen_feature_name = set([])
    for before_n_day in before_n_day_list:
        rs = pd.DataFrame()
        for now_day in days:
            if before_n_day > 0:
                selected_data = data[(data['type'] == 'train')
                                     & (data['context_timestamp_day'] >= now_day-before_n_day)
                                     & (data['context_timestamp_day'] < now_day)][cols]
            else:
                selected_data = data[data['type'] == 'train'][cols]

            count_name = '_'.join(cols[:-1])+'_'+str(before_n_day)+'d'+"_C"
            size_name = '_'.join(cols[:-1])+'_'+str(before_n_day)+'d'+"_I"
            pcvr_name = '_'.join(cols[:-1])+'_'+str(before_n_day)+'d'+"_pcvr"

            selected_data = selected_data.groupby(cols[:-1])
            count = selected_data.sum().reset_index()
            count.columns = cols[:-1]+[count_name]
            size = selected_data.size().reset_index()
            size.columns = cols[:-1]+[size_name]
            pcvr = selected_data.mean().reset_index()
            pcvr.columns = cols[:-1]+[pcvr_name]
            temp_rs = pd.concat(
                [count[cols[:-1]], count[count_name], size[size_name], pcvr[pcvr_name]], axis=1)

            # 计算平滑pcvr
            # hyper = BayesianSmoothing(1, 1)
            # hyper.update(count[count_name].values, size[size_name].values, 1000, 0.001)
            # alpha = hyper.alpha
            # beta = hyper.beta
            # temp_rs[pcvr_name] = (count[count_name] + alpha) / (size[size_name] + alpha + beta)

            temp_rs['context_timestamp_day'] = now_day

            rs = pd.concat([rs, temp_rs])
            gen_feature_name = gen_feature_name.union(
                [count_name, size_name, pcvr_name])
        data = pd.merge(data, rs, 'left', ['context_timestamp_day']+cols[:-1])
    return data, list(gen_feature_name)


def calc_count(data, col1, col2):
    count_name = col1+'_'+col2+'_count'
    count = data.groupby(col1)[col2].unique().map(lambda x: len(x))
    count = pd.DataFrame(count)
    count.columns = [count_name]
    count[col1] = count.index
    data = pd.merge(data, count, 'left', [col1])
    return data, [count_name]
