import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import scipy.special as special

import utils
from tqdm import tqdm

import preprocess1 as pre

class BayesianSmoothing(object):
    def __init__(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta

    def sample(self, alpha, beta, num, imp_upperbound):
        sample = numpy.random.beta(alpha, beta, num)
        I = []
        C = []
        for clk_rt in sample:
            imp = random.random() * imp_upperbound
            imp = imp_upperbound
            clk = imp * clk_rt
            I.append(imp)
            C.append(clk)
        return I, C

    def update(self, imps, clks, iter_num, epsilon):
        for i in range(iter_num):
            new_alpha, new_beta = self.__fixed_point_iteration(imps, clks, self.alpha, self.beta)
            if abs(new_alpha-self.alpha)<epsilon and abs(new_beta-self.beta)<epsilon:
                break
            #print (new_alpha,new_beta,i)
            self.alpha = new_alpha
            self.beta = new_beta

    def __fixed_point_iteration(self, imps, clks, alpha, beta):
        numerator_alpha = 0.0
        numerator_beta = 0.0
        denominator = 0.0

        for i in range(len(imps)):
            numerator_alpha += (special.digamma(clks[i]+alpha) - special.digamma(alpha))
            numerator_beta += (special.digamma(imps[i]-clks[i]+beta) - special.digamma(beta))
            denominator += (special.digamma(imps[i]+alpha+beta) - special.digamma(alpha+beta))

        return alpha*(numerator_alpha/denominator), beta*(numerator_beta/denominator)

def create_trade_precent_smooth(data,feature):
    """
    构造平滑商品历史成交转换率特征
    """
    feature_size = feature+"_I"
    feature_count = feature+"_C"
    feature_pcvr = feature+"_pcvr"

    I=data[data['type']=='train'].groupby(feature)['is_trade'].size().reset_index()
    I.columns = [feature, feature_size]
    C=data[data['type']=='train'].groupby(feature)['is_trade'].sum().reset_index()
    C.columns = [feature,feature_count]

    
    trade_precent = pd.concat([I[feature],I[feature_size], C[feature_count]], axis=1)
    hyper = BayesianSmoothing(1, 1)
    hyper.update(trade_precent[feature_size].values, trade_precent[feature_count].values, 6000, 0.000001)
    alpha = hyper.alpha
    beta = hyper.beta
    trade_precent[feature_pcvr] = (trade_precent[feature_count] + alpha) / (trade_precent[feature_size] + alpha + beta)

    # 直接使用平均数，不做平滑处理
    # pcvr=data[data['type']=='train'].groupby(feature)['is_trade'].mean().reset_index()
    # pcvr.columns = [feature,feature_pcvr] 
    # trade_precent = pd.concat([I[feature],I[feature_size], C[feature_count],pcvr[feature_pcvr]], axis=1) 

    data = pd.merge(data,trade_precent,'left',[feature])
    return data

def get_pcvr(data,feature_list):
    for feature in tqdm(feature_list):
        data = create_trade_precent_smooth(data,feature)
    return data

def get_cache_pcvr():
    data = utils.load_pickle(utils.temp_data_path+"pcvr.plk")
    return data

if __name__=="__main__":
    data = pre.get_all_data()

    # 需要计算pcvr的特征
    feature_list = ['item_category_1','item_category_2','item_brand_id','item_city_id','item_price_level','item_sales_level',
                    'item_collected_level','item_pv_level']
    feature_list.extend(['user_gender_id','user_age_level','user_occupation_id','user_star_level',])
    feature_list.extend(['context_timestamp_Hour','context_page_id',])
    feature_list.extend(['shop_review_num_level','shop_star_level'])

    data = get_pcvr(data,feature_list)

    feature_out = ['global_index']
    feature_out.extend([x+"_pcvr" for x in feature_list])
    feature_out.extend([x+'_I' for x in feature_list])
    feature_out.extend([x+'_C' for x in feature_list])

    utils.dump_pickle(data[feature_out],utils.temp_data_path+"pcvr.plk")
    utils.dump_pickle(True,utils.temp_data_path+"pcvr.update.plk")
