#Import libraries:
import pandas as pd
import numpy as np
import xgboost as xgb
from xgboost.sklearn import XGBClassifier
from sklearn import cross_validation, metrics   #Additional     scklearn functions
from sklearn.grid_search import GridSearchCV   #Perforing grid search

import matplotlib.pylab as plt
%matplotlib inline
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 12, 4




def modelfit(alg, dtrain, dtest ,predictors,offline=True,useTrainCV=True, cv_folds=5, early_stopping_rounds=50):
    """
    xgb训练函数
    offline=True 时表示用离线数据验证，打印测试集的logloss
    offline=False时生成提交文件,修改输出文件名
    useTrainCV=True时进行使用训练集进行交叉验证,输出当前参数n_estimators的最优值
    """
    ## CV验证
    if useTrainCV:
        xgb_param = alg.get_xgb_params()
        xgtrain = xgb.DMatrix(dtrain[predictors].values, label=dtrain[target].values)
        cvresult = xgb.cv(xgb_param, xgtrain, num_boost_round=alg.get_params()['n_estimators'], nfold=cv_folds,
                          metrics='logloss', early_stopping_rounds=early_stopping_rounds)
        print(cvresult.shape[0])
        alg.set_params(n_estimators=cvresult.shape[0])
        
    #训练模型
    alg.fit(dtrain[predictors], dtrain[target],eval_metric='logloss')
    #预测测试集,得到预测结果
    dtest['predicted_score'] = alg.predict_proba(dtest[predictors])[:,1]

    print ("\nModel Report")
    if offline :
        print ("logloss Score (Test): %f" % log_loss(dtest[target], dtest['predicted_score']))
    else:
        dtest[['instance_id', 'predicted_score']].to_csv('../result/xgb_4_2.txt', index=False,sep=' ')#保存在线提交结果
    
    
    feat_imp = pd.Series(alg.booster().get_fscore()).sort_values(ascending=False)
    feat_imp.plot(kind='bar', title='Feature Importances')
    plt.ylabel('Feature Importance Score')

    return dtest['predicted_score']



#读取特征工程后的数据
train_read = open('../data/sub_train_handled.pkl','rb')
train = pickle.load(train_read)
test_read = open('../data/sub_test_handled.pkl','rb')
test = pickle.load(test_read)

target = 'is_trade'
features = [## 商品基本特征
                'item_id', 'item_brand_id','item_city_id', 
                'item_price_level', 'item_sales_level',
                'item_collected_level', 'item_pv_level', 
                'category_last',
                ## 用户基本特征
                'user_gender_id', 'user_occupation_id',
                'user_age_level', 'user_star_level', 
                ## 上下文基本特征
                'context_page_id', 
                ## 商铺相关特征
                'shop_review_num_level', 'shop_star_level','shop_id',
                'shop_review_positive_rate', 'shop_score_service', 'shop_score_delivery', 'shop_score_description',
                ## 用户点击特征
                'user_query_day', 'user_query_day_hour',
                ## 时间相关特征
                'context_timestamp_Hour',
                ## 广告预测类别、属性的准确性
                'same_property_prob','predict_property_num','same_property_num',
                'same_category_prob','predict_category_num','same_category_num',
                ## 4.2 编码后的类别特征
                ## 类别编码
                'item_brand_id_enc','item_city_id_enc','shop_id_enc','category_last_enc',
                ## 是否需要编码？？ 会默认把缺失值当作一类分类处理
                'shop_star_level_enc','context_page_enc','user_occupation_id_enc',
                'user_star_level_enc','user_age_level_enc',
                

                ## 历史cvr转换率特征
                'category_pcvr','category_C','category_I',
                #'brand_pcvr','brand_C','brand_I',
                'city_pcvr','city_C','city_I',
                'price_pcvr','price_C','price_I',
                'sales_pcvr','sales_C','sales_I',
                'collected_pcvr','collected_C','collected_I',
                'pv_pcvr','pv_C','pv_I',
                'gender_pcvr','gender_C','gender_I',
                'age_pcvr','age_C','age_I',
                'occupation_pcvr','occupation_C','occupation_I',
                'userstar_pcvr','userstar_C','userstar_I',
                'hour_pcvr','hour_C','hour_I',
                'page_pcvr','page_C','page_I',
                'shop_review_pcvr','shop_review_C','shop_review_I',
                'shopstar_pcvr','shopstar_C','shopstar_I',
               ]


xgb = XGBClassifier(
    silent = 1,
    learning_rate =0.01,
    n_estimators=1282,
    max_depth=5,
    min_child_weight=7,
    gamma=0,
    subsample=0.7,
    colsample_bytree=0.7,
    objective= 'binary:logistic',
    scale_pos_weight=1,
    reg_alpha=0.001,
    seed=27)


xgb_prob=modelfit(xgb, train, test ,features,offline=False,useTrainCV=False)