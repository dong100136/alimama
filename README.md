在根目录下自己建一个data文件夹，下面在建temp和origin文件夹，一个放中间产生的文件，一个放原始的数据

这些东西太大了，不上传到gitlab上了。

## 参考资料

* [数据处理常用函数](https://blog.csdn.net/u010814042/article/details/74276452)
* [常用模型的简单框架](https://blog.csdn.net/Jinlong_Xu/article/details/78852099)
* [2017腾讯社交广告高校算法大赛](https://github.com/shenweichen/Tencent_Social_Ads2017_Mobile_App_pCVR)
* [Kaggle广告CTR预估比赛方案](https://blog.csdn.net/chengcheng1394/article/details/78940565)
* [GBDT+FFM解法方案](https://sunchenglong.gitbooks.io/kaggle-book/content/criteo-display-ad-challenge/FFM/3idiots-solution.html)
* [xgb模型调参GridSearch和RandomizedSearchCV](https://blog.csdn.net/liangjun_feng/article/details/79809037?spm=5176.9876270.0.0.11a52ef1uF5MG8)
* [别人分享的特征选择的代码](https://tianchi.aliyun.com/forum/new_articleDetail.html?spm=5176.8366600.0.0.7c7d311fMW4GCM&raceId=231647&postsId=4928#pages%3D3)

## 时间轴
* 2018.4.2 xgb模型优化后线上结果0.08233
* 2018.4.3 尝试实现ffm的baseline，使用[python封装的libffm库](https://github.com/keyunluo/python-ffm)
* 2018.4.4 利用python-ffm实现ffm算法的baseline,针对FFM进行输入特征的特殊处理
* 2018.4.7 将xgb和ffm模型简单融合得到了目前效果比较好的结果（0.6*xgb + 0.4*ffm）
* 2018.4.10 
   - 作死用20-24的数据去预测18号的成交率，结果死的很惨，同样的模型和参数logloss到了0.09xxx
   - 加了太多特征之后，ffm模型训练时间太慢
* 2018.4.11 提了几个trick特征，对于同一个user_id和item_id按照时间排序，最后一次点击的购买概率很大，而多次点击的情况下最后一次之前的购买概率很小。
线下提升至0.0806左右，效果十分明显。线上提交结果有小幅度提升，但是不如线下效果明显，应该还有改进的空间，可能是线下用到了穿越特性。实时排名到了365
## 堆特征

```
'item_query_day','item_query_day_hour',    #当前小时、天商品被点击数量
'shop_query_day','shop_query_day_hour',    #当前小时、天商铺被点击数量


----------------
tmp = train.groupby(['shop_id','item_id'], as_index=False)['instance_id'].agg({'item_cnt': 'count'})
tmp['item_cnt']=1
tmp=tmp.groupby(['shop_id'], as_index=False)['shop_id'].agg({'shop_item_cnt': 'count'})
----------------

brand_shop_cnt #一个brand有多少种打广告的shop
brand_item_cnt #一个brand有多少种打广告的item
brand_property_cnt
brand_category_cnt

shop_category_cnt #一个shop有多少种类别商品
shop_property_cnt  #一个shop有多少种不同属性的商品
shop_item_cnt  #一个shop有多少种打广告的item
----------------
tmp = train.groupby(['shop_id','user_age_level'], as_index=False)['instance_id'].agg({'shop_age_cnt': 'count'})
----------------
shop_age_favor #有多少同龄人点击了这个shop
shop_occupation_favor #有多少同职业人点击了这个shop
shop_starlevel_favor #有多少同等级人点击了这个shop
shop_gender_favor #有多少同性别人点击了这个shop

brand_age_favor #有多少同龄人点击了这个brand
brand_occupation_favor #有多少同职业人点击了这个brand
brand_starlevel_favor #有多少同等级人点击了这个brand
brand_gender_favor #有多少同性别人点击了这个brand

train.groupby(['shop_id']).mean()['item_sales_level']
shop_sale_level
shop_price_level
shop_collection_level
shop_pv_level

brand_sale_level
brand_price_level
brand_collection_level
brand_pv_level

```

